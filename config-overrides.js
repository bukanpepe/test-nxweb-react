module.exports = function override (config, env) {
    let loaders = config.resolve
    loaders.fallback = {
        "util": require.resolve("util/")
    }
    
    return config
}