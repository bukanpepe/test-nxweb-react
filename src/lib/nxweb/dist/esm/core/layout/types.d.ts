import type { ComponentType, PropsWithChildren, ReactNode } from 'react';
import type { RouteProps } from '../router/types.js';
export type LayoutProps = PropsWithChildren<{
    className?: string;
    meta?: Record<string, unknown>;
    name?: string;
    route?: RouteProps;
}>;
export type Layout = ComponentType<LayoutProps>;
export type LayoutWrapperProps = PropsWithChildren<{
    layout?: LayoutProps;
    route?: RouteProps;
}>;
export interface Layouts {
    get: (key: string) => Layout | undefined;
    has: (key: string) => boolean;
    layouts: ReadonlyMap<string, Layout>;
    names: readonly string[];
    register: (layouts: Record<string, Layout>) => void;
}
export type NavigationBadge = ReactNode | {
    className?: string;
    color?: string;
    content: ReactNode;
    pill?: boolean;
};
export interface NavigationHeader {
    badge?: NavigationBadge;
    className?: string;
    header: ReactNode;
    title?: string;
}
export interface NavigationLink {
    badge?: NavigationBadge;
    className?: string;
    disabled?: boolean;
    external?: boolean;
    icon?: ReactNode;
    id: string;
    link: string;
    text: ReactNode;
    title?: string;
}
export interface NavigationGroup extends Omit<NavigationLink, 'external' | 'link'> {
    children?: readonly (NavigationGroup | NavigationLink)[];
}
export type Navigation = NavigationGroup | NavigationHeader | NavigationLink;
