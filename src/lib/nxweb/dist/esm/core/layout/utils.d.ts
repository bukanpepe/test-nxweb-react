import type { ComponentType } from 'react';
import type { Layout, LayoutWrapperProps, NavigationGroup, NavigationLink } from './types.js';
import type { RouteProps } from '../router/types.js';
export declare const wraplayout: <T extends LayoutWrapperProps>(Layout: Layout, Wrapper: ComponentType<object>) => (props: T) => import("@emotion/react/jsx-runtime").JSX.Element;
export declare const interpolateLinkParams: (s: string, params?: Readonly<Record<string, unknown>>) => string;
export declare const isNavigationLinkActive: (link: string, currentURL: string, routeProps?: RouteProps, params?: Readonly<Record<string, unknown>>) => boolean;
export declare const isNavigationGroupActive: (children: readonly (NavigationGroup | NavigationLink)[] | undefined, currentURL: string, routeProps?: RouteProps) => boolean;
export declare const findNavigation: (navigation: readonly (NavigationGroup | NavigationLink)[], currentURL: string, routeProps?: RouteProps) => string[] | undefined;
