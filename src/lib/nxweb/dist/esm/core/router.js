/*! react v3.6.0+52b37aca | Copyright (c) 2023 NextPlatform. All rights reserved. */
export{BrowserRouter,HashRouter,MemoryRouter}from"./router/router.js";export{resolveRoutes}from"./router/utils.js";