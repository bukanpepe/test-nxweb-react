import type { ComponentType } from 'react';
export type Wrapper<T> = (component: ComponentType<T>) => ComponentType<T>;
export declare function WithWrapper<T>(component: ComponentType<T>, ...wrapper: Wrapper<T>[]): ComponentType<T>;
