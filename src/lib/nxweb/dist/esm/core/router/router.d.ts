import type { FC } from 'react';
import type { MemoryRouterProps, RouterProps } from './types.js';
declare const BrowserRouter: FC<RouterProps>;
declare const HashRouter: FC<RouterProps>;
declare const MemoryRouter: FC<MemoryRouterProps>;
export { BrowserRouter, HashRouter, MemoryRouter };
