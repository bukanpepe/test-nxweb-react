import type { ComponentType, ReactNode } from 'react';
import type { RouteObject } from 'react-router-dom';
import type { Route, RouteProps } from './types.js';
import type { LayoutProps, Layouts } from '../layout/types.js';
export declare const resolveRoutes: (routes: readonly Route[], layouts?: Layouts, wrapper?: ComponentType<{
    layout?: LayoutProps;
    route?: RouteProps;
}>, defaultLayout?: string, fallback?: ReactNode) => RouteObject[];
