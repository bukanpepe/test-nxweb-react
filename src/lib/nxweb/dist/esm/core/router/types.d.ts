import type { ComponentType, ReactNode } from 'react';
import type { URLSearchParamsInit } from 'react-router-dom';
import type { LayoutProps, Layouts } from '../layout/types.js';
export declare type RouteParams<T extends string = string> = {
    readonly [key in T]: string | undefined;
};
export interface RouteFunctionArgs {
    context?: unknown;
    params: RouteParams;
    request: Request;
}
export type LoaderFunction = (args: RouteFunctionArgs) => Promise<Response> | Promise<unknown> | Response;
export type ActionFunction = (args: RouteFunctionArgs) => Promise<Response> | Promise<unknown> | Response;
export interface RouteProps {
    className?: string;
    meta?: Record<string, unknown>;
    path: string;
    title?: string;
}
export interface IndexRoute {
    action?: ActionFunction;
    caseSensitive?: boolean;
    children?: undefined;
    element?: ComponentType<RouteProps>;
    error?: ReactNode | null;
    index: true;
    loader?: LoaderFunction;
    redirectTo?: undefined;
    title?: string;
}
export interface NonIndexRoute {
    action?: ActionFunction;
    caseSensitive?: boolean;
    children?: (IndexRoute | NonIndexRoute)[];
    element?: ComponentType<RouteProps>;
    error?: ReactNode | null;
    index?: false;
    loader?: LoaderFunction;
    path: string;
    redirectTo?: undefined;
    title?: string;
}
export interface RedirectRoute {
    hash?: string | true;
    navigateTo: string;
    path: string;
    replace?: boolean;
    search?: URLSearchParamsInit | true;
}
export type Route = RouteProps & {
    layout?: LayoutProps | string;
    suspended?: boolean;
} & (IndexRoute | NonIndexRoute | RedirectRoute);
export interface RouterProps {
    basePath?: string;
    defaultLayout?: string;
    fallback?: ReactNode;
    layouts?: Layouts;
    routes?: Route[];
    wrapper?: ComponentType<{
        layout?: LayoutProps;
        route?: RouteProps;
    }>;
}
export interface MemoryRouterProps extends RouterProps {
    initialEntries?: string[];
    initialIndex?: number;
}
