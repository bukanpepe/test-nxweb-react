/*! react v3.6.0+52b37aca | Copyright (c) 2023 NextPlatform. All rights reserved. */
export function WithWrapper(e,...r){return r.reverse().reduce(((e,r)=>r(e)),e)}