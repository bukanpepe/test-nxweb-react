import type { AuthState } from '@nxweb/auth';
import type { AuthAction } from './types.js';
/**
 * Handles how that state changes in the `useAuth` hook.
 */
export declare const AuthReducer: (state: AuthState, action: AuthAction) => AuthState;
