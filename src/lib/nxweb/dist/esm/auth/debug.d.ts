import type { FC } from 'react';
import type { AuthConfig } from '@nxweb/auth';
interface AuthConfigEditorProps {
    className?: string;
    config: Partial<AuthConfig>;
    onChange?: (config: Partial<AuthConfig>, platform: string) => void;
    platform?: string;
    platforms?: readonly string[];
}
declare const AuthConfigEditor: FC<AuthConfigEditorProps>;
export type { AuthConfigEditorProps };
export { AuthConfigEditor };
