import type { FC } from 'react';
import type { ProviderProps } from 'react-redux';
import type { Action, Store } from 'redux';
import type { ThunkDispatch } from 'redux-thunk';
export declare function createStoreHook<TModel extends object, TAction extends Action, TArg = unknown>(): <TState = unknown>(selector: (state: TModel) => TState) => [TState, ThunkDispatch<TModel, TArg, TAction>];
export declare function createStoreProvider<TState, TAction extends Action>(store: Readonly<Store<TState, TAction>>): FC<Omit<ProviderProps, 'store'>>;
export declare function createCommandHook<TCommand>(command: TCommand): <T = unknown>(selector: (cmd: TCommand) => T) => T;
