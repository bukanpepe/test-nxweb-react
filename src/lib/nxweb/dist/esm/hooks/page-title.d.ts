export declare const PageTitle = "NextWeb Application";
export declare const usePageTitle: ({ separator, appName }?: {
    separator?: string;
    appName?: string;
}) => [string, (value: string | ((s: string) => string)) => void];
