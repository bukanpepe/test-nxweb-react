import type { DependencyList } from 'react';
export type DebouncedFunc = (...args: readonly any[]) => void;
export declare const useDebounce: <T extends DebouncedFunc>(func: T, deps: DependencyList, delay?: number) => void;
