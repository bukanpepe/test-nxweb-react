import type { DependencyList, EffectCallback } from 'react';
export declare const useDeepCompare: <T = unknown>(value: T) => T;
export declare const useDeepEffect: (effect: EffectCallback, dependencies?: DependencyList) => void;
