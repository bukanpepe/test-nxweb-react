export type PageEmbeddedHandler = (embedded: boolean) => void;
export interface PageEmbeddedOptions {
    paramName?: string;
}
export declare const usePageEmbedded: (handler?: PageEmbeddedHandler, options?: Readonly<PageEmbeddedOptions>) => boolean;
