import type { RefObject } from 'react';
export declare const useCopyToClipboard: (callback: (value?: string) => void, ref: RefObject<HTMLElement>) => (() => void);
