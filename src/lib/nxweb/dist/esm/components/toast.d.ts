import type { ComponentClass, CSSProperties, FC, MouseEvent, ReactElement, ReactNode, RefAttributes, RefObject } from 'react';
import type { ToastContent as Content } from 'react-toastify';
type ToastID = number | string;
type ToastDraggableDirection = 'x' | 'y';
type ToastPosition = 'bottom-center' | 'bottom-left' | 'bottom-right' | 'top-center' | 'top-left' | 'top-right';
type ToastType = 'default' | 'error' | 'info' | 'success' | 'warning';
type ToastTheme = 'colored' | 'dark' | 'light';
interface ToastIconProps {
    theme: ToastTheme;
    type: ToastType;
}
type ToastIcon = ReactElement<ToastIconProps> | ReactNode | ((props: Readonly<ToastIconProps>) => ReactNode);
interface ToastTransitionProps {
    children?: ReactNode;
    done: () => void;
    isIn: boolean;
    nodeRef: RefObject<HTMLElement>;
    position: ToastPosition;
    preventExitTransition: boolean;
}
type ToastTransition = ComponentClass<ToastTransitionProps> | FC<ToastTransitionProps>;
type ToastClassName = string | ((context?: Readonly<{
    defaultClassName?: string;
    position?: ToastPosition;
    rtl?: boolean;
    type?: ToastType;
}>) => string);
interface ToastCloseButtonProps {
    ariaLabel?: string;
    closeToast: (e: MouseEvent<HTMLElement>) => void;
    theme: ToastTheme;
    type: ToastType;
}
interface ToastCommonOptions {
    /**
     * Set the delay in ms to close the toast automatically.
     * Use `false` to prevent the toast from closing.
     * `Default: 5000`
     */
    autoClose?: number | false;
    /**
     * An optional css class to set for the toast content.
     */
    bodyClassName?: ToastClassName;
    /**
     * An optional inline style to apply for the toast content.
     */
    bodyStyle?: CSSProperties;
    /**
     * Pass a custom close button.
     * To remove the close button pass `false`
     */
    closeButton?: ReactElement<ToastCloseButtonProps> | boolean | ((props: Readonly<ToastCloseButtonProps>) => ReactNode);
    /**
     * Remove the toast when clicked.
     * `Default: true`
     */
    closeOnClick?: boolean;
    /**
     * Set id to handle multiple container
     */
    containerId?: ToastID;
    /**
     * Allow toast to be draggable
     * `Default: true`
     */
    draggable?: boolean;
    /**
     * Specify in which direction should you swipe to dismiss the toast
     * `Default: "x"`
     */
    draggableDirection?: ToastDraggableDirection;
    /**
     * The percentage of the toast's width it takes for a drag to dismiss a toast
     * `Default: 80`
     */
    draggablePercent?: number;
    /**
     * Hide or show the progress bar.
     * `Default: false`
     */
    hideProgressBar?: boolean;
    /**
     * Used to display a custom icon. Set it to `false` to prevent
     * the icons from being displayed
     */
    icon?: ToastIcon;
    /**
     * Pause the toast when the window loses focus.
     * `Default: true`
     */
    pauseOnFocusLoss?: boolean;
    /**
     * Pause the timer when the mouse hover the toast.
     * `Default: true`
     */
    pauseOnHover?: boolean;
    /**
     * Set the default position to use.
     * `One of: 'top-right', 'top-center', 'top-left', 'bottom-right', 'bottom-center', 'bottom-left'`
     * `Default: 'top-right'`
     */
    position?: ToastPosition;
    /**
     * An optional css class to set for the progress bar.
     */
    progressClassName?: ToastClassName;
    /**
     * An optional style to set for the progress bar.
     */
    progressStyle?: CSSProperties;
    /**
     * Define the ARIA role for the toast
     * `Default: alert`
     *  https://www.w3.org/WAI/PF/aria/roles
     */
    role?: string;
    /**
     * Support right to left display.
     * `Default: false`
     */
    rtl?: boolean;
    /**
     * Theme to use.
     * `One of: 'light', 'dark', 'colored'`
     * `Default: 'light'`
     */
    theme?: ToastTheme;
    /**
     * Pass a custom transition built with react-transition-group.
     */
    transition?: ToastTransition;
}
interface ToastOptions<TData = object> extends ToastCommonOptions {
    /**
     * An optional css class to set.
     */
    className?: ToastClassName;
    /**
     * The toast data
     */
    data?: TData;
    /**
     * Add a delay in ms before the toast appear.
     */
    delay?: number;
    /**
     * Whether toast is loading
     */
    isLoading?: boolean;
    /**
     * Set the percentage for the controlled progress bar. `Value must be between 0 and 1.`
     */
    progress?: number | string;
    /**
     * An optional inline style to apply.
     */
    style?: CSSProperties;
    /**
     * Set a custom `toastId`
     */
    toastId?: ToastID;
    /**
     * Set the toast type.
     * `One of: 'info', 'success', 'warning', 'error', 'default'`
     */
    type?: ToastType;
    /**
     * Used during update
     */
    updateId?: ToastID;
}
type ToastContent<T> = Content<T>;
declare const Toast: {
    show<T>(message: ToastContent<T>, options?: ToastOptions): void;
    success<T_1>(message: ToastContent<T_1>, options?: ToastOptions): void;
    error<T_2>(message: ToastContent<T_2>, options?: ToastOptions): void;
    warn<T_3>(message: ToastContent<T_3>, options?: ToastOptions): void;
    info<T_4>(message: ToastContent<T_4>, options?: ToastOptions): void;
};
interface ToastContainerProps extends ToastCommonOptions {
    /**
     * An optional css class to set.
     */
    className?: ToastClassName;
    /**
     * Show the toast only if it includes containerId and it's the same as containerId
     * `Default: false`
     */
    enableMultiContainer?: boolean;
    /**
     * Limit the number of toast displayed at the same time
     */
    limit?: number;
    /**
     * Whether or not to display the newest toast on top.
     * `Default: false`
     */
    newestOnTop?: boolean;
    /**
     * An optional inline style to apply.
     */
    style?: React.CSSProperties;
    /**
     * An optional css class for the toast.
     */
    toastClassName?: ToastClassName;
    /**
     * An optional inline style to apply for the toast.
     */
    toastStyle?: React.CSSProperties;
}
declare const ToastContainer: FC<RefAttributes<HTMLDivElement> & ToastContainerProps>;
export type { ToastID, ToastDraggableDirection, ToastPosition, ToastType, ToastTheme, ToastIcon, ToastIconProps, ToastTransition, ToastTransitionProps, ToastClassName, ToastCloseButtonProps, ToastCommonOptions, ToastOptions, ToastContent, ToastContainerProps };
export { Toast, ToastContainer };
