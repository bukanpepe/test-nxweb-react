import type { FC, ReactNode } from 'react';
interface PageSpinnerProps {
    backdrop?: boolean;
    className?: string;
    logo?: ReactNode;
}
declare const PageSpinner: FC<PageSpinnerProps> & {
    defaultLogo?: string;
};
interface LoadingSpinnerProps {
    className?: string;
    message?: ReactNode;
}
declare const LoadingSpinner: FC<LoadingSpinnerProps>;
export type { PageSpinnerProps as FallbackSpinnerProps, LoadingSpinnerProps };
export { PageSpinner, LoadingSpinner };
