import type { FC, PropsWithChildren } from 'react';
interface TextProps extends PropsWithChildren {
    variant?: 'body-1' | 'body-2' | 'body-3' | 'caption' | 'heading-large' | 'heading-medium' | 'sub-heading';
    weight?: 'bold' | 'medium' | 'regular';
}
declare const Text: FC<TextProps>;
export { Text };
