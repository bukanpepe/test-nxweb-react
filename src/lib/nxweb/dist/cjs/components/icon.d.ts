import type { FC, SVGAttributes } from 'react';
interface IconProps extends SVGAttributes<SVGElement> {
    color?: string;
    size?: number | string;
    src: string;
    svgClassName?: string;
    title?: string;
}
declare const Icon: FC<IconProps>;
export type { IconProps };
export { Icon };
