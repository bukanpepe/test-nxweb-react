import type { FC, ReactNode } from 'react';
interface ScrollTopProps {
    children?: ReactNode;
    className?: string;
    scrollBehaviour?: ScrollBehavior;
    showOffset?: number;
}
declare const ScrollTop: FC<ScrollTopProps>;
export type { ScrollTopProps };
export { ScrollTop };
