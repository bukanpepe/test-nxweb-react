import type { Auth, AuthAPI, AuthClient, AuthData, AuthState, RedirectCallbackResult } from '@nxweb/auth';
/**
 * The action types used in the `useAuth` hook.
 */
export type AuthAction = {
    data?: Auth;
    type: 'AUTHORIZE_COMPLETE' | 'GET_ACCESS_TOKEN_COMPLETE' | 'HANDLE_REDIRECT_COMPLETE' | 'LOGIN_MANUAL' | 'LOGIN_POPUP_COMPLETE';
} | {
    error: Error;
    type: 'ERROR';
} | {
    type: 'LOGIN_POPUP_STARTED';
} | {
    type: 'LOGOUT';
};
/**
 * Contains the authenticated state and authentication methods provided by the `useAuth` hook.
 */
export type AuthClientAPI = AuthAPI & AuthClient & Readonly<AuthState> & {
    login: (auth: Readonly<AuthData>) => void;
};
/**
 * The state of the application before the user was redirected to the login page.
 */
export type RedirectState<T> = RedirectCallbackResult<T> & {
    returnTo?: string;
};
export type PathPattern = Readonly<{
    exact?: boolean;
    path: RegExp | string;
}> | RegExp | string;
