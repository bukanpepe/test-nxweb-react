import type { Context, FC, ReactNode } from 'react';
import type { AuthClient, AuthConfig } from '@nxweb/auth';
import type { Cache, CacheLocation } from '@nxweb/auth/cache';
import type { Logger } from '@nxweb/auth/logger';
import type { Platform } from '@nxweb/auth/utils';
import type { AuthClientAPI, PathPattern, RedirectState } from './types.js';
/**
 * The Auth Context
 */
declare const AuthContext: Context<AuthClientAPI>;
interface AuthProviderProps {
    cache?: Cache | CacheLocation;
    children: ReactNode | ((auth: AuthClientAPI, path: string) => ReactNode);
    client?: AuthClient | ((opts: AuthConfig) => AuthClient);
    config: AuthConfig;
    debug?: boolean;
    disabled?: boolean | ((path: string) => boolean);
    error?: ReactNode | ((auth: AuthClientAPI, path: string) => ReactNode);
    /**
     * Exclude certain paths from triggering authorization.
     * If the current path exist in exclude list the authorization flow
     * will be skipped for that path
     *
     * ```jsx
     * <AuthProvider
     *   config={config}
     *   exclude={['/login']}
     * >
     * ```
     */
    exclude?: PathPattern[];
    ignoreCache?: boolean;
    loader?: ReactNode;
    logger?: Logger;
    manual?: boolean;
    onConfigChange?: (config: AuthConfig) => void;
    onInit?: (api: AuthClientAPI, config: AuthConfig) => void;
    /**
     * By default this removes the code and state parameters from the url when you are redirected from the authorize page.
     * It uses `window.history` but you might want to overwrite this if you are using a custom router, like `react-router-dom`
     * See the EXAMPLES.md for more info.
     */
    onRedirect?: <T>(state?: Readonly<RedirectState<T>>) => void;
    platform?: Platform;
    popup?: boolean;
    /**
     * By default, if the page url has code/state params, the SDK will attempt to exchange the
     * code for a token. In some cases the code might be for something else (another OAuth SDK perhaps). In these
     * instances you can instruct the client to ignore them eg
     *
     * ```jsx
     * <AuthProvider
     *   config={config}
     *   skipRedirect={window.location.pathname === '/stripe-oauth-callback'}
     * >
     * ```
     */
    skipRedirect?: boolean;
}
declare const AuthProvider: FC<AuthProviderProps>;
declare const useAuth: () => AuthClientAPI;
export type { AuthProviderProps };
export { AuthContext, AuthProvider, useAuth };
