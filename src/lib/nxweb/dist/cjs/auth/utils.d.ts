import type { PathPattern } from './types.js';
export declare const hasAuthParams: (searchParams?: string) => boolean;
export declare const resolvePathPatterns: (patterns?: PathPattern[]) => RegExp[];
