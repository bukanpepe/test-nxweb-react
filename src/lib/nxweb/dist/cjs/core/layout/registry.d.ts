import type { Layout, Layouts } from './types.js';
export declare class LayoutRegistry implements Layouts {
    #private;
    constructor(layouts?: Record<string, Layout>);
    get layouts(): ReadonlyMap<string, Layout>;
    get names(): readonly string[];
    get(key: string): Layout | undefined;
    has(key: string): boolean;
    register(layouts: Record<string, Layout>): void;
}
