import type { RefObject } from 'react';
export declare function useMounted(): RefObject<boolean>;
