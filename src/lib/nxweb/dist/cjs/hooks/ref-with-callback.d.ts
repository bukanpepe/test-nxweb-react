export declare const useRefWithCallback: (onMount?: (ref: unknown) => void, onUnmount?: (ref: unknown) => void) => (node: any) => void;
