import type { RefObject } from 'react';
export declare function getFullscreenElement(): "fullscreenElement" | "mozFullScreenElement" | "msFullscreenElement" | "webkitFullscreenElement";
export declare function useFullscreen(ref: RefObject<HTMLElement>): [boolean, () => Promise<boolean>];
