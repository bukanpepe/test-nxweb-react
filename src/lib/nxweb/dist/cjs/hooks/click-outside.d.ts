import type { MutableRefObject } from 'react';
export declare const useClickOutside: <T extends HTMLElement>(ref: MutableRefObject<T>, handler: (e: MouseEvent | TouchEvent) => void) => void;
