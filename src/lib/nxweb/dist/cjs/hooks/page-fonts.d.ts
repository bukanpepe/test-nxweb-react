export declare const PageFonts: Set<string>;
export declare const usePageFonts: (...additionals: readonly string[]) => string[];
