# Changelog

All notable changes to this project will be documented in this file.

## [3.6.0-rc.0] ｜ 2023-02-07

### ✨ Features

- [`55384f05`](https://gitlab.com/nxweb/react/-/commit/55384f05250a7ec07d91631604f778fae59db586) initial commit

[3.6.0-rc.0]: https://gitlab.com/nxweb/react/-/tags/v3.6.0-rc.0 "3.6.0-rc.0"
