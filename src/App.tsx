import './App.css';

import { Text } from './lib/nxweb/dist/esm/components'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Text variant="heading-large" weight='bold'>Heading Large Bold</Text>
        <Text variant="heading-large" weight="medium">Heading Large Medium</Text>
        <Text variant="heading-large">Heading Large Regular</Text>
        <br />
        <Text variant="heading-medium" weight='bold'>Heading Medium Bold</Text>
        <Text variant="heading-medium" weight="medium">Heading Medium Medium</Text>
        <Text variant="heading-medium">Heading Medium Regular</Text>
        <br />
        <Text variant="sub-heading" weight='bold'> Sub-Heading Bold</Text>
        <Text variant="sub-heading" weight="medium"> Sub-Heading Medium</Text>
        <Text variant="sub-heading"> Sub-Heading Regular</Text>
        <br />
        <Text weight='bold'> Body 1 Bold</Text>
        <Text weight="medium"> Body 1 Medium</Text>
        <Text> Body 1 Regular</Text>
        <br />
        <Text variant="body-2" weight='bold'> Body 2 Bold</Text>
        <Text variant="body-2" weight="medium"> Body 2 Medium</Text>
        <Text variant="body-2"> Body 2 Regular</Text>
        <br />
        <Text variant="body-3" weight='bold'> Body 3 Bold</Text>
        <Text variant="body-3" weight="medium"> Body 3 Medium</Text>
        <Text variant="body-3"> Body 3 Regular</Text>
        <br />
        <Text variant="caption" weight='bold'>Caption Bold</Text>
        <Text variant="caption" weight="medium">Caption Medium</Text>
        <Text variant="caption">Caption Regular</Text>
        <br />
      </header>
    </div>
  );
}

export default App;
